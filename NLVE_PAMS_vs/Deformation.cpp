// Deformation.cpp

#include "Deformation.h"

using namespace std;

Deformation::Deformation() {};

Deformation::Deformation(double_t t_in, double_t Ri_in, double_t Ro_in,
		double_t L_in, double_t lambda_z_in, double_t ri_in,
		double_t Beta_in) : t(t_in), Ri(Ri_in), Ro(Ro_in), L(L_in),
		lambda_z(lambda_z_in), ri(ri_in), Beta(Beta_in) {
	//fiber angle and direction
	//Beta = Betai;

	// deformation gradient


	// right cauchy-green strain tensor
	//Eigen::Matrix<double_t, 3, 3> C = defGrad * defGrad;

	// Invariants
}

array<double_t, 7> Deformation::getParams() {
	array<double_t, 7> out = { t,Ri,Ro,L,lambda_z,ri,Beta };
	
	return out;
};

Eigen::Matrix<double_t, 3, 3> Deformation::getDefGrad(double_t R) {
	Eigen::Matrix<double_t, 3, 3> defGrad
		= Eigen::Matrix<double_t, 3, 3>::Zero();
	
	double_t sqrtTerm = sqrt(pow(ri, 2) + (pow(R, 2) - 1) / lambda_z);
	defGrad(0, 0) = R / (lambda_z*sqrtTerm);
	defGrad(1, 1) = sqrtTerm / R;
	defGrad(2, 2) = lambda_z;

	Eigen::Matrix<double_t, 3, 3> out = defGrad;
	
	return out;
};

Eigen::Matrix<double_t, 3, 3> Deformation::getC(double_t R) {
	Eigen::Matrix<double_t, 3, 3> out = getDefGrad(R)*getDefGrad(R);

	return out;
};

Eigen::Matrix<double_t, 3, 1> Deformation::getL() {
	Eigen::Matrix<double_t, 3, 1>betaVecPos
		= Eigen::Matrix<double_t, 3, 1>::Zero();

	betaVecPos(0, 0) = 0;
	betaVecPos(1, 0) = sin(Beta);
	betaVecPos(2, 0) = cos(Beta);

	Eigen::Matrix<double_t, 3, 1> out = betaVecPos;
	
	return out;
};

array<double_t, 3> Deformation::getInvariants(double_t R) {
	Eigen::Matrix<double_t, 3, 3> C = getC(R);
	double_t I1 = C.trace(); //Invariant I1

	Eigen::Matrix<double_t, 3, 3> Csquared = C * C;
	double_t I2 = 0.5 * (C.trace()*C.trace() - Csquared.trace()); //Invariant I2	

	Eigen::Matrix<double_t, 3, 1> L = getL();
	double_t I4 = L.transpose() * C * L; //Invariant I4

	array<double_t, 3> out = { I1, I2, I4 };
	
	return out;
};
