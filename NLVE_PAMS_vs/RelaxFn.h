// RelaxFn.h

#pragma once

#include <array>
#include <vector>
#include <numeric>
#include <iostream>
#include<iomanip>

#include "Eigen/Core"
#include "math.h"
#include "AlphaFn.h"


class RelaxFn {
private:
	int num;

public:
	RelaxFn(int);
	
	Eigen::Matrix<double_t, 3, 3> evaluateRelaxFn(double_t, Deformation,
		double_t, G1dim &, G1dim &, G1dim &, AlphaFn &);
	
	Eigen::Matrix<double_t, 3, 3> evaluateRelaxFnDot(double_t, Deformation,
		double_t, G1dim &, G1dim &, G1dim &, AlphaFn &);
	
	Eigen::Matrix<double_t, 3, 3> integrateTimeTrapRelaxFnDot(
		unsigned int, double_t, std::vector<Deformation> &, double_t, G1dim &,
		G1dim &, G1dim &, AlphaFn &);
};
