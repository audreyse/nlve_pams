#include <array>
#include "Eigen/Core"
#include "math.h"

class DefGrad {
private:
	lambda_z; // axial stretch
	ri; // interior radius
	R; // 
public:
	DefGrad(double_t g0, const double_t gInf, const double_t t) :
		gt0(g0),
		gtInf(gInf),
		tau(t) {
	}; //make the function
	double_t g1dimEval(double_t);
	std::array<double_t, 3> getParams();
	//evaluate the function
	//double_t g1dimIntegrate;
};

// evaluate the G function
double_t G1dim::g1dimEval(double_t t) {
	return gtInf + (gt0 - gtInf)*exp(-t / tau);

};
