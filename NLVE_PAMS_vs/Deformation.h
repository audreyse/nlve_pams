// Deformation.h

#pragma once
#include <array>
#include "Eigen/Core"
#include "math.h"

class Deformation {
private:
	double_t t; // time stamp

	double_t Ri; // interior radius in load-free config
	double_t Ro; // exterior radius in load-free config
	double_t L; //length in load-free config
	
	/* initial fiber angle , defined from the tube's central axis, of one
	fiber family. Other fiber famiy is assumed -beta. */
	double_t Beta;

	//tangent vector of initial config fiber in R,Theta,Z.
	//Eigen::Matrix<double_t, 3, 1> betaVecPos;
	
	//tangent vector of other fiber
	//Eigen::Matrix<double_t, 3, 1> betaVecNe;

	double_t lambda_z; // axial stretch
	double_t ri; // interior radius in current config

	//Eigen::Matrix<double_t, 3, 3> defGrad; //deformation gradient
	//Eigen::Matrix<double_t, 3, 3> C; //right cauchy-green strain tensor
	//std::array<double_t, 3> InvariantList;
	//double_t I1;
	//double_t I2;
	//double_t I4;

public:
	// Default constructor
	Deformation();

	Deformation(double_t, double_t, double_t, double_t, double_t, double_t,
				double_t);

	std::array<double_t, 7> getParams();
	Eigen::Matrix<double_t, 3, 3> getDefGrad(double_t);
	Eigen::Matrix<double_t, 3, 3> getC(double_t);
	Eigen::Matrix<double_t, 3, 1> getL();
	std::array<double_t, 3> getInvariants(double_t);
};
