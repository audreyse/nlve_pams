// ConstEq.cpp

#include "ConstEq.h"
using namespace std;

ConstEq::ConstEq(int nu) : num(nu) {}

int ConstEq::getNum() {
	return num;
}

// Evaluate right hand side of Eqn. 2.6
Eigen::Matrix<double_t, 3, 3> ConstEq::evaluateConstEq(
		unsigned int n, double_t &q, double_t t,
	std::vector<Deformation> &dVec, double_t R, G1dim &g1, G1dim &g2, G1dim &g3,
		AlphaFn &alph) {

	Eigen::Matrix<double_t, 3, 3> eye
		= Eigen::Matrix<double_t, 3, 3>::Identity();

	Eigen::Matrix<double_t, 3, 3> constVecQ= evaluateConstEqNoQ(n,t,dVec,R,g1,g2,g3,alph) - eye * q;
	return constVecQ;
}

// evaluate F Pi F^T part of constitutive equation. Does not include hydrostatic Lagrange multiplier.
	Eigen::Matrix<double_t, 3, 3> ConstEq::evaluateConstEqNoQ(unsigned int n,
		double_t t, std::vector<Deformation>&dVec, double_t R, G1dim &g1, G1dim &g2, G1dim &g3,
		AlphaFn &alph) {

		std::vector<Eigen::Matrix<double_t, 3, 3>> exc(n + 1);

		RelaxFn relFn = RelaxFn(1);

		//evaluate 1st term of Eq. 2.7
		Eigen::Matrix<double_t, 3, 3> relFnCurrent = relFn.evaluateRelaxFn(0, dVec[n], R, g1, g2, g3, alph);

		//evaluate F Pi F^t
		Eigen::Matrix<double_t, 3, 3> F = dVec[n].getDefGrad(R);
		Eigen::Matrix<double_t, 3, 3> sigma = F
			* (relFn.integrateTimeTrapRelaxFnDot(n, t, dVec, R, g1, g2, g3, alph) + relFnCurrent)*F.transpose();
		return sigma;
	}


// evaluate space integral kernel for hydrostatic pressure, i.e. intergral kernel of B.8 of paper.
double_t ConstEq::evaluateIntKernel(unsigned int &n, double_t &t,
	std::vector<Deformation> &dVec, double_t R, G1dim &g1, G1dim &g2, G1dim &g3,
		AlphaFn &alph) {

	//std::cout << "n read into eval int kernel is " << n << "\n";
	//std::cout << "t read into eval int kernel is " << t << "\n";                                                        

	if (t > 0) {
	//	std::cout << "t in evaluateIntKernel is nonzero \n";
	};

	Eigen::Matrix<double_t, 3, 3> sigma = evaluateConstEqNoQ(n, t, dVec, R, g1, g2, g3, alph);

	array<double_t, 7>dVec0 = dVec[n].getParams();
	double_t ri = dVec0[5];
	double_t Ri = dVec0[1];
	double_t Ro = dVec0[2];
	double_t lz = dVec0[4];

	Eigen::Matrix<double_t, 3, 3> sigmaR = sigma;
	double_t Frr = sigmaR(0, 0);
	double_t Ftt = sigmaR(1, 1);
	//double_t R = Ri+(Ro-Ri)/(i+1);

	double_t r = sqrt(ri*ri+(R*R - Ri*Ri) / lz);
	//std::cout << "r is " << r << "\n";
	double_t Q = (Frr - Ftt) / ((1.0/R)*r*r*lz);
	//std::cout << "R is " << R << "\n";
	//std::cout << "Frr is " << Frr << "\n";
	//std::cout << "Ftt is  " << Ftt << "\n";

	double_t doubleOut = Q;

	return doubleOut;
}

//evaluate RHS of Eqn. 4.1 (Equivalent to B.8) of paper, using trapezoidal rule
double_t ConstEq::hydroPIntegralRHS(unsigned int &nt, unsigned int &nr,
		double_t &t, std::vector<Deformation> &dVec, G1dim &g1, G1dim &g2, G1dim &g3,
		AlphaFn &alph) {

	double_t time = t;
	//std::cout << "time in hydroPIntegralRHS is " << t << "\n";
	double_t ntD = nt;

	std::vector<double_t> out(nt+1);

	std::array<double_t, 7> dVec0 = dVec[0].getParams();

	double_t Ri = dVec0[1];
	double_t Ro = dVec0[2];

	std::vector<double_t> kernels(nr+1);

	double_t nrd = nr;

	//std::cout << "nrd is " << nrd << "\n";
	
	for (unsigned int i = 0; i <= nr; i++) {
		//std::cout << "inside radius loop iter n." << i << "\n";
		//std::cout << "i is " << i << "\n";
		//std::cout << "3rd arg for int kernel is " << Ri + i / (nrd + 1) * (Ro - Ri) << "\n";
		
		double_t id = i;
		//std::cout << "j is " << j << "\n";
		double_t kVal = evaluateIntKernel(nt, t, dVec, Ri + id/(nrd+1) * (Ro - Ri), g1, g2, g3, alph);
		kernels[i] = kVal;
		//std::cout << "kernel matrix value " << kVal << "\n";
		//std::cout << "kernel matrix total " << kernels << "\n";
	};

	//std::cout << "max time is " << nt << "\n";
	//std::cout << "max time is " << nt << "\n";
	//std::cout << "final kernel matrix gives" << kernels << "\n";
	//std::cout << "ns passed in is " << nt << "\n";
	double_t k2sum = accumulate(kernels.begin(),kernels.end(),0.0f);
	double_t ans = (Ro-Ri)/(nrd+1)*(k2sum-kernels[0]/2-kernels[nr]/2);
	//std::cout << "ans is" << ans << "\n";
	return ans;
}

double_t ConstEq::pressureDifference(double_t P, double_t rhs) {
	double_t out = -P-rhs;
	return out;
}

// Solve for r_i at current time such that Eqn. B.8 (equivalent to 4.1) is satisfied. Uses secant metod
std::vector<double_t> ConstEq::solveHydroPSecantMethod(unsigned int &nt, 
	unsigned int &nr, double_t &t, std::vector<Deformation> &dVec, G1dim &g1,
	G1dim &g2, G1dim &g3, AlphaFn &alph, std::vector<double_t> &pressures,
	double_t precision, double_t guessRange) {
	
	std::vector<double_t> out(nt + 1);

	//std::vector<std::vector<Deformation>> dVecGuess(1);
	std::vector<Deformation> dVecHistory;

	for (unsigned int i = 0; i <= nt; i++) {
		//std::cout << "\n entered time loop " << i << "\n";
		std::array<double_t, 7> currentDef = dVec[i].getParams(); // get imposed deformation parameters
		//std::cout << "got deformation parameters at a time made initial deformation guess \n";

		double_t xn2 = 0.9; //currentDef[5] - guessRange;
		double_t xn1 = 2.2;// currentDef[5] + guessRange;

		if (i != 0) {
			std::array<double_t, 7> prevDef = dVec[i - 1].getParams(); // get imposed deformation parameters
			xn2 = prevDef[5] - guessRange;
			xn1 = prevDef[5] + guessRange;
			//std::cout << "using prev radius " << prevDef[5] << "\n";
		}
		
		// make deformation histories, populated with appropriate radius guesses
		Deformation dGuessA = Deformation(currentDef[0], currentDef[1], currentDef[2], currentDef[3], currentDef[4], xn2, currentDef[6]);
		Deformation dGuessB = Deformation(currentDef[0], currentDef[1], currentDef[2], currentDef[3], currentDef[4], xn1, currentDef[6]);
		//std::cout << "made initial deformation guess \n";

		std::vector<Deformation> dVecGuessA = dVecHistory;
		dVecGuessA.push_back(dGuessA);

		std::vector<Deformation> dVecGuessB = dVecHistory;
		dVecGuessB.push_back(dGuessB);
		
		//"until now" time vectors
		double_t in = i;
		double_t times = t;
		double_t incrs = nt;
		double_t s = in * times / (incrs + 1);
		unsigned int ns = i;
		//std::cout << "current time step " << s << "\n";
		//std::cout << "ns is " << ns << "\n";

		//compute RHS of hydrostatic equilibrium equation at this timestep for Guess A and Guess B
		double_t rhsA = hydroPIntegralRHS(ns, nr, s, dVecGuessA, g1, g2, g3, alph);
		//std::cout << "made initial rhs guess A \n";
		double_t rhsB = hydroPIntegralRHS(ns, nr, s, dVecGuessB, g1, g2, g3, alph);
		//std::cout << "made initial rhs guess B \n";


		std::vector<double_t>rhsVec = { rhsA,rhsB };

		//std::cout << "rhs vals are " << rhsA << " " << rhsB << "\n";
		//std::cout << "rhsVec is " << rhsVec << "\n";

		double_t P = pressures[i];
		double_t q0 =pressureDifference(P, rhsVec[0]); //initial difference between 2 sides of eqn
		double_t q = pressureDifference(P, rhsVec[1]);

		std::vector<Deformation> dVecGuesses(2);
		double_t xn = 1;

		//std::cout << "initial q is " << q << "\n";

		while(abs(q)>precision) {

			//std::cout << "entered while loop, guess is " << xn << "\n";

			//step through 
			xn = xn1-((pressureDifference(P,rhsVec[1])*(xn1-xn2))/(pressureDifference(P,rhsVec[1])-pressureDifference(P,rhsVec[0])));

			// update the right hand side (rhs) vector with new radius guess
			Deformation dGuess = Deformation(currentDef[0], currentDef[1], currentDef[2], currentDef[3], currentDef[4], xn, currentDef[6]); //make deformation object with new radius guess
			std::vector<Deformation> dVecGuess = dVecHistory;
			dVecGuess.push_back(dGuess);

			//std::cout << "next dVecGuess is " << xn << "\n";

			double_t rhsGuess = hydroPIntegralRHS(ns, nr, s, dVecGuess, g1, g2, g3, alph); //evaluate the RHS with new deformation object. should have length 1
			//std::cout << "P is " << P << "\n";
			//std::cout << "q is " << q << "\n";
			//std::cout << "r guess is" << xn << "\n";
			//std::cout << "rhsGuess is" << rhsGuess << "\n";
			
			
			//update error
			q =pressureDifference(P,rhsGuess);

			//push back rhs guesses for next time
			rhsVec[0] = rhsVec[1];
			rhsVec[1] = rhsGuess;

			//step back ri guesses
			xn2 = xn1;
			xn1 = xn;
			
		}
		dVecHistory.push_back(Deformation(currentDef[0], currentDef[1], currentDef[2], currentDef[3], currentDef[4], xn, currentDef[6]));
		dVec[i] = Deformation(currentDef[0], currentDef[1], currentDef[2], currentDef[3], currentDef[4], xn, currentDef[6]);
		out[i] = xn;
		//std::cout << "final q is " << q << "\n";
		//std::cout << "xn is " << xn << "\n";
	};
	return out;
}

// Find total en force on the PAM (Eqn. 4.2)
std::vector<double_t> ConstEq::getResultantForce(unsigned int &nt,
	unsigned int &nr, double_t &t, std::vector<Deformation> &dVec, G1dim &g1,
	G1dim &g2, G1dim &g3, AlphaFn &alph, std::vector<double_t> &pressures) {
	
	std::vector<double_t> out(nt + 1);

	for (unsigned int i = 0; i <= nt; i++) {
		double_t P = pressures[i];
		//std::cout << "P is " << P << "\n";
		std::array<double_t,7> defParams = dVec[i].getParams();
		double_t Ri = defParams[1];
		double_t Ro = defParams[2];
		double_t ri = defParams[5];
		double_t lN = defParams[4];

		unsigned int s = i;

		//std::cout << "s is " << s << "\n";
		std::vector<double_t> qVec(nr+1);
		std::vector<double_t> kernelVec(nr + 1);

		std::vector<Deformation> dV(i+1);

		if (i > 0){
			for (unsigned int k = 0; k <= i; k++) {
				dV[k] = dVec[k];
			}
		}
		else {
			dV[i] = dVec[0];
		}
		
		double_t times = i;
		double_t incrs = nt;
		double_t time = times*t/ (incrs + 1);
		//std::cout << "t passed in to getResultantforce is " << t << "\n";
		//std::cout << "time calculated is " << time << "\n";

		for (unsigned int j = 0; j <= nr; j++) {
			double_t dj = j;
			double_t nrdbl = nr;
			double_t R = dj/(nrdbl+1)*(Ro-Ri)+Ri;
			Eigen::Matrix<double_t, 3, 3> cEq = evaluateConstEqNoQ(s, time, dV, R, g1, g2, g3, alph);
			double_t qVal = cEq(0, 0) - P + hydroPIntegralRHS(s, nr, time, dV, g1, g2, g3, alph);
			qVec[j] = qVal;
			double_t kV = R*(1.0/lN)*(-qVec[j] + cEq(2, 2));
			//std::cout << std::setprecision(20) << "ceq is " << cEq(2,2) << "\n";
			//std::cout << std::setprecision(20) << "kV is " << kV << "\n";
			kernelVec[j] = kV;
		}
		double_t kernelRTSum = std::accumulate(kernelVec.begin(),kernelVec.end(),0.0);
		//std::cout << std::setprecision(20)  << "integral value " << kernelRTSum << "\n";
		double_t nrd = nr;
		double_t forceAns = (kernelRTSum - kernelVec[0] / 2.0 - kernelVec[nr] / 2.0)*(Ro - Ri) / (nrd + 1);
		double_t piD = 3.1415926535897932384626433;
		out[i] = 2*piD*forceAns-P*ri*ri*piD;
		std::cout << "forces calc iter no. " << i << "\n";
	};
	return out;
}
