// g1dim.h

#pragma once
#include <array>
#include "Eigen/Core"
#include "math.h"

class G1dim {
private:
	const double_t gt0; // G_a (0)
	const double_t gtInf; // G_a(Inf)
	const double_t tau; // time constant

public:
	G1dim(double_t, double_t, double_t);
	//make the function
	double_t g1dimEval(double_t);
	double_t gDotEval(double_t);
	std::array<double_t,3> getParams();
	//evaluate the function
};
