// AlphaFn.h

#pragma once

#include <array>
#include "Eigen/Core"
#include "math.h"

#include "Deformation.h"
#include "g1dim.h"

class AlphaFn{
private:
	double_t a; // coefficient in Phi

public:
	// Constructor
	AlphaFn(double_t);

	//methods to evaluate alpha functions

	double_t phiEval(Deformation,double_t);

	std::array<double_t, 4> evaluateAlpha(double_t, Deformation, double_t,
		G1dim, G1dim, G1dim);
	
	std::array<double_t, 4> evaluateAlphaDot(double_t, Deformation, double_t,
		G1dim, G1dim, G1dim);
};
