// RelaxFn.cpp

#include "RelaxFn.h"

using namespace std;

RelaxFn::RelaxFn(int nu) : num(nu) {}

Eigen::Matrix<double_t, 3, 3> RelaxFn::evaluateRelaxFn(double_t t,
		Deformation d, double_t R, G1dim &g1, G1dim &g2, G1dim &g3, AlphaFn &a) {

	array<double_t, 4> aArr = a.evaluateAlpha(t, d, R, g1, g2, g3);
	array<double_t, 4> aDotArr = a.evaluateAlphaDot(t, d, R, g1, g2, g3);

	double_t a0 = get<0>(aArr);
	double_t a1 = get<1>(aArr);
	double_t a2 = get<2>(aArr);
	double_t a3 = get<3>(aArr);

	Eigen::Matrix<double_t, 3, 3> C = d.getC(R);
	Eigen::Matrix<double_t, 3, 1> L = d.getL();

	Eigen::Matrix < double_t, 3, 3 > out = a0 * Eigen::Matrix<double_t, 3, 3>::Identity() + a1 * C + a2 * C*C + a3 * L*L.transpose();

	return out;
}

//evaluate d/d(t-s) R[C(s),t-s]
Eigen::Matrix<double_t, 3, 3> RelaxFn::evaluateRelaxFnDot(double_t t,
		Deformation d, double_t R, G1dim &g1, G1dim &g2, G1dim &g3, AlphaFn &a) {

	// d relates to C(s)
	// t will be passed in as t-s later

	array<double_t, 4> aDotArr = a.evaluateAlphaDot(t, d, R, g1, g2, g3); //is this wrong? t-s

	double_t a0d = get<0>(aDotArr);
	double_t a1d = get<1>(aDotArr);
	double_t a2d = get<2>(aDotArr);
	double_t a3d = get<3>(aDotArr);

	Eigen::Matrix<double_t, 3, 3> C = d.getC(R);
	Eigen::Matrix<double_t, 3, 1> L = d.getL();

	Eigen::Matrix < double_t, 3, 3 > out
		= a0d * Eigen::Matrix<double_t, 3, 3>::Identity() + a1d * C + a2d
		* C*C + a3d * L*L.transpose();

	return out;
}

//integrate from s=0 to s=1 d/d(t-s) R[C(s),t-s] ds
Eigen::Matrix<double_t, 3, 3> RelaxFn::integrateTimeTrapRelaxFnDot(
	unsigned int n, double_t t, std::vector<Deformation> &dVec, double_t R,
	G1dim &g1, G1dim &g2, G1dim &g3, AlphaFn &a) {

	std::vector<Eigen::Matrix<double_t, 3, 3>> timeKernels(n + 1);
	double_t nd = n;

	for (unsigned int i = 0; i <= n; i++) {
		double_t id = i;

		Eigen::Matrix<double_t, 3, 3> rDotValue = evaluateRelaxFnDot(t - (id*t / (nd + 1)), dVec[i], R, g1, g2, g3, a); //what should 1st entry be? right now I think it's t-s
		timeKernels[i] = rDotValue;
	}


	Eigen::Matrix<double_t, 3, 3> sum = Eigen::Matrix<double_t, 3, 3>::Zero();

	for (unsigned int i = 0; i <= n; i++) {
		sum = sum + timeKernels[i];
	}

	Eigen::Matrix<double_t, 3, 3> ans = t / (nd + 1)*(sum - 0.5*timeKernels[0] - 0.5*timeKernels[n]);

	return ans;
}