/*
This file contains the 'main' function.
Program execution begins and ends there.
*/

// NLVE_PAMS_vs.cpp : This file contains the 'main' function. Program execution begins and ends there.

//#include "pch.h"
#include "math.h"
#include "Eigen/Core"

#include "ConstEq.h"
#include "AlphaFn.h"
#include "RelaxFn.h"
#include "Deformation.h"
#include "g1dim.h"

#include <iostream>
#include <iomanip>  
#include <algorithm>
#include <fstream>
#include <string>



int main(int argc, char**argv)
{
	unsigned int n = 640; //number of steps in time
	unsigned int nr = 80; //number of steps in the radius
	double_t t = 8; // time (in time constants)


	/*Below: vectors containing model parameters. 
	*Values can be changed to evaluate the model with different design, material, or deformation properties.
	*/
	
	// a vector with values of Gm_inf (Eq. 2.16) 
	std::vector<double_t> g1Infs(2);
	g1Infs[0] = 1;
	g1Infs[1] = 0.75;

	// a vector with values of tau_f/tau_m (Eq. 2.16-2.19)
	std::vector<double_t> g3ts(5);
	g3ts[0] = 0.1;
	g3ts[1] = 1;
	g3ts[2] = 10;
	g3ts[3] = 0.3;
	g3ts[4] = 3;

	// a vector with fiber orientation values -- called Gamma in the paper, but Beta in the code (Eq. 3.11) 
	std::vector<double_t> Betas(5);
	Betas[0] = 0.349066;
	Betas[1] = 0.698132;
	Betas[2] = 1.0472;
	Betas[3] = 1.39626;
	Betas[4] = 1.571;

	// a vector of imposed lambda_z (Eq. 3.3)
	std::vector<double_t> lzs(3);
	lzs[0] = 0.92;
	lzs[1] = 1;
	lzs[2] = 1.05;
	/*for (unsigned int ps = 0; ps <= 250; ps++) {
		double_t psd = ps;
		lzs[ps] = 0.85 + psd * 0.0012;
	}*/

	// a vector of pressure frequencies -- used for sinusoidal pressure inputs.
	std::vector<double_t> pfs(6);
	pfs[0] = 0.75;
	pfs[1] = 0.95;
	pfs[2] = 0.55;
	pfs[3] = 1.15;
	pfs[4] = 0.35;
	pfs[5] = 1.35;

	/* Per Section 2 of paper, the Mooney-Rivlin term G2(t) is neglected. 
	* Thus it is implemented here with 0 stiffness at t=0 and t->Inf 
	*/
	G1dim g2(0, 0, 1);

	/* Maximum number of combinations of parameters that can be evaluated in one run of code. 
	* Used so that file names can be reserved for each case.
	*/
	unsigned int totalRuns = 1005;
	std::vector<std::string> filenames(totalRuns);

	for (unsigned int k = 0; k <= totalRuns - 1; k++) {
		filenames[k] = "out_" + std::to_string(k) + ".csv";
	};


	/* Nested for loops so that each combination of parameters is evaluated and results are saved to a separate file. 
	* Change the limits of the for loop to run only specific cases within the vectors defined above.
	*/
	unsigned int counter = 0;
	for (unsigned int m = 1; m <= 1; m++) { //gm_Infs
		//Initialize function for G_m(t)
		G1dim g1(1, g1Infs[m], 1);

		for (unsigned int o = 2; o <= 4; o++) { //pressure freqs
			double_t b = pfs[o];
			double_t a = 0.25;

			for (unsigned int u = 1; u <= 1; u++) { //lambda_z
				double_t lz = lzs[u];

				for (unsigned int v = 0; v <= 0; v++) {//fiber angle Gamma
					double_t Beta = Betas[v];

					for (unsigned int w = 1; w <=1; w++) { //tau_f/tau_m
						double_t g3tau = g3ts[w];
						
						counter++;
						
						// Initialize function for G_f(t)
						G1dim g3(10, 8, g3tau);
						

						std::string filename = filenames[counter];
						std::cout << "now on" << filename << "\n";
						
						// Make deformation object (functions as defined in Eqns. 3.1-3.3). Initial outer radius R_o is input as 1.2.
						Deformation d1(0, 1, 1.2, 1, lz, 1, Beta);

						/*
						* Initializes variables for R (between R_i and R_o), 
						* relaxation functions Alpha (Eqns. 2.11-2.14), 
						* and tensorial relaxation function R (as simplified in Appedix A).
						* Correct values calculated later in the code.
						*/
						double_t R = 1.2;
						AlphaFn a1(1);
						RelaxFn r1(1);

						// Initialize vector to store deformations over time.
						std::vector<Deformation> dVec(n + 1);
						for (unsigned int i = 0; i <= n; i++) {
							dVec[i] = d1;
						};

						std::cout << "F Pi Ft \n";
						ConstEq cEq = ConstEq(1);

						std::ofstream fs;

						std::array<double_t, 3> g1Params = g1.getParams();
						std::array<double_t, 3> g3Params = g3.getParams();

						// Open file and write down input parameters, material parameters, and design parameters
						fs.open(filename);

						fs << "G10," << g1Params[0] << "\n";
						fs << "G1Inf," << g1Params[1] << "\n";
						fs << "tau_1," << g1Params[2] << "\n";

						fs << "G30," << g3Params[0] << "\n";
						fs << "G3Inf," << g3Params[1] << "\n";
						fs << "tau_3," << g3Params[2] << "\n";

						fs << "beta," << Beta << "\n";
						fs << "lz," << lz << "\n";

						fs << "time," << "pressure," << "force," << "radius" << "\n";


						int nPres = 0;

						for (unsigned int j = 0; j <= nPres; j++) {
							//std::cout << "making new pressure vector \n";
							std::vector<double_t> pressures(n + 1);
							std::fill_n(pressures.begin(), n + 1, 0.1);
							for (unsigned int i = 0; i <= n; ++i) {

								double_t c = 0.2;
								double_t time = static_cast<double>(i) * static_cast<double>(t) / (static_cast<double>(n) + 1);

								//input for step pressure:
								//pressures[i] = 0.15;

								
								//input for one period of sine wave pressure; amplitude a:
								pressures[i] = 0;
								double n8 = n;
								if (i < 2 * ((n8 + 1) / t)*(1 / b)) {
								 pressures[i] = a / 2.0 + a / 2.0 * sin(b*3.1415926535897932384626433*time - 3.1415929 / 2.0);
								}

							};

							// Find r_i(t) using secant method and trapezoidal rules with Eqn. 4.1 
							std::vector<double_t> radByTime = cEq.solveHydroPSecantMethod(n, nr, t, dVec, g1, g2, g3, a1, pressures, 0.000001, 0.25);
							std::cout << "\n";


							// Find t (current time, over time) from max time and number of time steps above
							std::vector<double_t> timeVec(n + 1);
							double_t dn = n;
							for (unsigned int i = 0; i <= n; i++) {
								double_t di = i;
								timeVec[i] = di * t / (dn + 1);
							}

							// Find f(t) using previous results for r_i(t), deformation, inputs, and material properties
							std::vector<double_t> forceByTime = cEq.getResultantForce(n, nr, t, dVec, g1, g2, g3, a1, pressures);
							
							/* Save values found into file: each row corresponds to one time step and has the time, internal pressure, 
							* force, and r_i saved.
							*/
							for (unsigned int i = 0; i <= n; i++) {
								fs << timeVec[i] << "," << pressures[i] << "," << forceByTime[i] << "," << radByTime[i] << "\n";
							}

						}
						fs.close();
					}
				}
			}

		}
	}

}

