// ConstEq.h

#include "RelaxFn.h"
#include <numeric>

class ConstEq {

private:
	int num;

public:
	// Constructor
	ConstEq(int);

	// Get function for private member variable
	int getNum();

	Eigen::Matrix<double_t, 3, 3> evaluateConstEq(
		unsigned int, double_t &, double_t,
		std::vector<Deformation>&, double_t, G1dim &, G1dim &, G1dim &, AlphaFn &);

	Eigen::Matrix<double_t, 3, 3> evaluateConstEqNoQ(unsigned int,
		double_t, std::vector<Deformation>&, double_t, G1dim &, G1dim &, G1dim &,
		AlphaFn &);

	double_t evaluateIntKernel(unsigned int &nt, double_t &t,
		std::vector<Deformation> &dVec, double_t R, G1dim &g1, G1dim &g2, 
		G1dim &g3, AlphaFn &alph);

	double_t hydroPIntegralRHS(unsigned int &, unsigned int &, double_t &,
		std::vector<Deformation> &, G1dim &, G1dim &, G1dim &, AlphaFn &);

	double_t pressureDifference(double_t P, double_t rhs);
	
	//std::vector<double_t> hydroPIntegralRHSVec(unsigned int nt, unsigned int nr, double_t t, std::vector<Deformation> dVec, G1dim g1, G1dim g2, G1dim g3, AlphaFn alph);
	
	std::vector<double_t> solveHydroPSecantMethod(unsigned int &nt, 
		unsigned int &nr, double_t &t, std::vector<Deformation> &dVec, 
		G1dim &g1, G1dim &g2, G1dim &g3, AlphaFn &alph, std::vector<double_t> &pressures, 
		double_t precision, double_t guessRange);

	std::vector<double_t> solveHydroPTable(unsigned int & nt, unsigned int & nr, double_t & t, std::vector<Deformation>& dVec, G1dim & g1, G1dim & g2, G1dim & g3, AlphaFn & alph, std::vector<double_t>& pressures, double_t precision, double_t guessRange);

	std::vector<double_t> getResultantForce(unsigned int & nt, unsigned int & nr, 
		double_t & t, std::vector<Deformation>& dVec, G1dim & g1, G1dim & g2, G1dim & g3, 
		AlphaFn & alph, std::vector<double_t>& pressures);
};
