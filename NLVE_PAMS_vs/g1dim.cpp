// g1dim.cpp

#include "g1dim.h"

using namespace std;

G1dim::G1dim(double_t g0, double_t gInf, double_t t)
	: gt0(g0), gtInf(gInf), tau(t) {}

// evaluate the G function
double_t G1dim::g1dimEval(double_t t) {
	double_t out = gtInf + (gt0 - gtInf)*exp(-t / tau);
	return out;
	//std::cout << "G is " << out << "\n";
}

// evaluate G dot
double_t G1dim::gDotEval(double_t t) {
	double_t out = -(gt0 - gtInf) / tau * exp(-t / tau);
	return out; 
	//std::cout << "G dot is " << out << "\n";
}

// get the parameters in the G function
std::array<double_t, 3> G1dim::getParams() {
	std::array<double_t, 3> out = { gt0,gtInf,tau };
	return out;
}
