// AlphaFn.cpp

#include "AlphaFn.h"
#include <iostream>
#include <iomanip>
#include <numeric>


using namespace std;

AlphaFn::AlphaFn(double_t a_in) : a(a_in) {}

double_t AlphaFn::phiEval(Deformation d, double_t R) {
	double_t I1 = get<0>(d.getInvariants(R));
	double_t Phi = exp(-a * (I1 - 3.0));
	
	return Phi;
};

array<double_t, 4> AlphaFn::evaluateAlpha(double_t t, Deformation d,
											   double_t R, G1dim g1, G1dim g2,
											   G1dim g3) {
	double_t I1 = get<0>(d.getInvariants(R));
	double_t I2 = get<1>(d.getInvariants(R));
	double_t I4 = get<2>(d.getInvariants(R));

	double_t Phi = exp(-a * (I1 - 3));

	double_t a0 = 2.0 * g1.g1dimEval(t)*(1 - I2 * Phi) + 2.0
				  * g2.g1dimEval(t)*(I1 - 2.0 * I2*Phi);
	double_t a1 = 2.0 * g1.g1dimEval(t)*I1*Phi + 2.0 * g2.g1dimEval(t)
				  * (2.0 * I1*Phi - 1.0);
	double_t a2 = -2.0 * Phi*(g1.g1dimEval(t) + 2.0 * g2.g1dimEval(t));
	double_t a3 = 2*(I4 - 1.0)*g3.g1dimEval(t);

	//std::cout << std::setprecision(10) << "t is " << t << "\n";
	//std::cout << "g1 1 is " << g1.g1dimEval(t) << "\n";

	array<double_t, 4> out = { a0, a1, a2, a3 };
	
	return out;
};

array<double_t, 4> AlphaFn::evaluateAlphaDot(double_t t, Deformation d,
											 double_t R, G1dim g1, G1dim g2,
											 G1dim g3) {
	double_t I1 = get<0>(d.getInvariants(R));
	double_t I2 = get<1>(d.getInvariants(R));
	double_t I4 = get<2>(d.getInvariants(R));

	double_t Phi = exp(-a * (I1 - 3.0));

	// what happens if I give deformation as a function of time?

	double_t a0 = 2.0 * g1.gDotEval(t)*(1 - I2 * Phi) + 2.0 * g2.gDotEval(t)
				  * (I1 - 2.0 * I2*Phi);
	double_t a1 = 2.0 * g1.gDotEval(t)*I1*Phi + 2.0 * g2.gDotEval(t)
				  * (2.0 * I1*Phi - 1.0);
	double_t a2 = -2.0 * Phi*(g1.gDotEval(t) + 2.0 * g2.gDotEval(t));
	double_t a3 = 2*(I4 - 1.0)*g3.gDotEval(t);

	array<double_t, 4> out = { a0, a1, a2, a3 };
	
	return out;
};
