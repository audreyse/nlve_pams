# Code for "Force reversal and energy dissipation in composite tubes through nonlinear viscoelasticity of component materials," Proc. Royal Soc. A, 2020.

# Authors: Audrey Sedal and Alan Wineman

## Requirements

### Windows 64-bit
If you are running a 64-bit version of windows, make sure your Active Solution Platform is x64.
This program was last tested in Windows Visual Studio 2019.
Open "NLVE_PAMS_vs/NLVE_PAMS_vs.sln" to run. Run with option "O2" for increased speed.

### Eigen
Eigen is required to build the code. The code is tested with Eigen 3.3.5.
* http://eigen.tuxfamily.org

## Troubleshooting

### Windows SDK Version
If you run into issues where math.h is not found, make sure the Windows SDK version is correct.